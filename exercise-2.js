const houses = [];

var gryffindor = {
    houseName: "Gryffindor",
    founder: "Godric Gryffindor",
    members: [
        "Harry Potter",
        "Hermione Granger",
        "Ron Weasley",
        "Neville Longbottom",
    ],
};
var hufflepuff = {
    houseName: "Hufflepuff",
    founder: "Helga Hufflepuff",
    members: [
        "Cedric Diggory",
        "Justin Finch-Fletchley",
        "Pomona Sprout",
        "Silvanus Kettleburn",
    ],
};

var ravenclaw = {
    houseName: "Ravenclaw",
    founder: "Rowena Ravenclaw",
    members: [
        "Luna Lovegood",
        "Cho Chang",
        "Gilderoy Lockhart",
        "Garrick Ollivander",
    ],
};

var slytherin = {
    houseName: "Slytherin",
    founder: "Salazar Slytherin",
    members: ["Draco Malfoy", "Severus Snape", "Tom Ryddle", "Severus Snape"],
};

var newMembers = [
    {
        houseName: "Gryffindor",
        name: "Álvaro Fuentenebro",
    },
    {
        houseName: "Hufflepuff",
        name: "Javier Esclapes",
    },
    {
        houseName: "Ravenclaw",
        name: "Ariel Rivas",
    },
    {
        houseName: "Slytherin",
        name: "Sara Rodriguez",
    },
    {
        houseName: "Hufflepuff",
        name: "Rocío Aguilera",
    },
    {
        houseName: "Gryffindor",
        name: "Manuela Gutierrez",
    },
    {
        houseName: "Slytherin",
        name: "Esther Ávila",
    },
    {
        houseName: "Ravenclaw",
        name: "Enzo Nardone",
    },
];


var school = {
    name: "Howarts",
    createHouse: function () {
        var house = {};
        house.name = this.houseName;
        house.members = this.members;
        return house;
    },
};

// ---- Solo puedes programar a partir de aquí ----

function createHouse(house) {
    return school.createHouse.call(house);
}

function addHouses() {
    var args = Array.prototype.slice.call(arguments);
    for (const house of args) {
        houses.push(house);
    }

}

function searchCharacter(initialLetter) {
    const selectedMembers = [];
    for (const house of houses) {
        for (const member of house.members) {
            if (member.charAt(0) == initialLetter) {
                selectedMembers.push(member);
            }
        }
    }
    return selectedMembers;
}

function addMember(member) {
    for (const newMember of newMembers) {
        if (newMember.name === member) {
            currentHouse = newMember.houseName;
            break;
        }
    }
    for (const house of houses) {
        if (house.name === currentHouse) {
            house.members.push(member);
            return `>>>>>>>>> Añadido ${member} en ${house.name}`; 
        }
    }
}

function addFounder(houseFounder) {
    let founderName = '';
    switch (houseFounder) {
        case 'Slytherin':
            founderName = slytherin.founder;
            break;
        case 'Hufflepuff':
            founderName = hufflepuff.founder;
            break;
        case 'Ravenclaw':
            founderName = ravenclaw.founder;
            break;
        case 'Gryffindor':
            founderName = gryffindor.founder;
            break;
        default:
            break;
    }
    if (founderName) {
        for (const house of houses) {
            if (house.name === houseFounder) {
                house.founder = founderName;
                return `>>>>>>>>> ${founderName} añadido a ${houseFounder}`;
            }
        }
    } else {
        return 'nombre de House erronea';
    }
}

// Iteration 1 - Your code here
const createGryfindor = createHouse(gryffindor);
const createHufflepuff = createHouse(hufflepuff);
const createRavenclaw = createHouse(ravenclaw);
const createSlytherin = createHouse(slytherin); 

// Iteration 2 - Your code here
addHouses(createGryfindor,createHufflepuff,createRavenclaw,createSlytherin);
console.log('NEWHOUSES', houses);
// Iteration 3 - Your code here
console.log('Miembros con letra G', searchCharacter('G'));
console.log('Miembros con letra H', searchCharacter('H'));
console.log('Miembros con letra C', searchCharacter('C'));

// Iteration 4 - Your code here
console.log(addMember('Álvaro Fuentenebro'));
console.log(houses);

// Iteration 5 - Your code here
console.log(addFounder('Slytherin'));
console.log(houses);


